'use strict';

angular.module('myApp.carousel', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/carousel', {
    templateUrl: 'views/carousel/carousel.html',
    controller: 'CarouselCtrl'
  });
}])

.controller('CarouselCtrl', [function() {

}]);
